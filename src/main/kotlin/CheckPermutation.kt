fun String.isPermutation(s: String): Boolean {
    fun isPermutation(): Boolean {
        val map: MutableMap<Char, Int> = mutableMapOf()
        fun modifyCharacterCount(c: Char, value: Int) {
            val current = map[c] ?: 0
            map[c] = current + value
            if (map[c] == 0) map.remove(c)
        }

        this.forEach { char -> modifyCharacterCount(char, +1) }
        s.forEach { char -> modifyCharacterCount(char, -1) }
        return map.isEmpty()
    }

    return when {
        this == s -> true
        this.length != s.length -> false
        else -> isPermutation()
    }
}