import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class Test {
    @Test fun `Ensure that the empty strings are permutations`() {
        assertTrue("".isPermutation("")) { "The empty string is not a permutation of itself!" }
    }

    @Test fun `Ensure that strings of different lengths are not permutations`() {
        val string1 = "a"
        val string2 = "aa"
        assertFalse(string1.isPermutation(string2)) { "Two stings of different lengths cannot be permutations!" }
    }

    @Test fun `Ensure that strings of the same lengths are permutations`() {
        val string1 = "abb"
        val string2 = "bab"
        assertTrue(string1.isPermutation(string2)) { "A valid permutation is not recognized!" }
    }

    @Test fun `Ensure that strings of the same lengths are not permutations`() {
        val string1 = "aab"
        val string2 = "abb"
        assertFalse(string1.isPermutation(string2)) { "An invalid permutation is not recognized!" }
    }
}